package com.retail.core;

public class GroundShippingMethod implements ShippingMethod{

	@Override
	public double getItemShippingCost(Item item) {
		// TODO Auto-generated method stub
		return Math.round(item.getWeight() * 2.5 * 100.0)/100.0;
	}

	@Override
	public double getItemShippingCostWithTariff(Item item) {
		return this.getItemShippingCost(item)+((item.getPrice()*3)/100.0);
	}

	
	
	

}
