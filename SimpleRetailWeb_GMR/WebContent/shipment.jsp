<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome to shipment page</title>
</head>

<!-- LIST OF ITEMS SHIPPED -->

<body>
	<form method='POST' action='products'>
		<table border="1">
			<c:forEach items="${items}" var="item">
				<tr>
					<td><input type="checkbox" name="shipmentRequired"
						value="${item.upc}"><br></td>
					<td><c:out value="${item.upc}" /></td>
					<td><c:out value="${item.description}" /></td>
					<td><c:out value="${item.price}" /></td>
					<td><c:out value="${item.weight}" /></td>
					<td><c:out value="${item.shipMethod}" /></td>

					
				</tr>
			</c:forEach>
		</table>
		<br> <br> <br> <input type='submit'
			value='Get Items to be Shipped' />
	</form>

	

</body>
</html>