<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Items Shipped</title>
</head>
<body>

<!-- LIST OF ITEMS AVAILABLE FOR SHIPMENT. CHOOSE THE REQUIRED ONES FROM THEM -->
<form method='GET' action='products'>
	<table border="1">
		<c:forEach items="${itemShippedList}" var="item">
			<tr>
				
				<td><c:out value="${item.upc}" /></td>
				<td><c:out value="${item.description}" /></td>
				<td><c:out value="${item.price}" /></td>
				<td><c:out value="${item.weight}" /></td>
				<td><c:out value="${item.shipMethod}" /></td>
				<td><c:out value="${item.shippingCost}" /></td>
			</tr>
		</c:forEach>
		<tr><td colspan="5">Total Shipping Cost: </td> <td width="10%">${totalShippingCost}</td> </tr>
	</table>
	<br>
	<br>
	<br>
		<input type='submit' value='Home' />
	</form>
</body>
</html>