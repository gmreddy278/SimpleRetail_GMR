package com.retail.core.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.retail.core.commoms.Item;
import com.retail.core.commoms.Product;

public class ProductController extends HttpServlet {

	Product p = new Product();
	
	//LIST TO STORE AVAILBLE ITEMS
	List<Item> items = p.getProductsInfo();
	//LIST TO STORE SHIPPED ITEMS
	List<Item> itemShippedList = new ArrayList<>();

	//METHOD TO GET ALL AVAILABLE ITEMS
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("items", items);

		RequestDispatcher rd = request.getRequestDispatcher("shipment.jsp");
		rd.forward(request, response);
	}

	// METHOD THAT RETURN THE SHIPPED ITEMS WITH TOTAL COST
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		if (request.getParameterValues("shipmentRequired")!= null) {
			System.out.println(request.getParameterValues("shipmentRequired").length);
			
		String[] upcList = request.getParameterValues("shipmentRequired");
			List<String> upsList = Arrays.asList(upcList);

			List<Item> toRemove = new ArrayList<>();
			for (Item i : items) {
				System.out.println(" For:  " + i.getUpc().toString());
				if (upsList.contains(i.getUpc().toString())) {
					Item.totalShippingCost+= i.getShippingCost();
					System.out.println(i.getUpc().toString());
					System.out.println("i = " + i);
					itemShippedList.add(i);
					toRemove.add(i);
					
				}

			}
			items.removeAll(toRemove);
		}
		System.out.println("Item.getTotalShippingCost(): "+Item.getTotalShippingCost());
		request.setAttribute("itemShippedList", itemShippedList);
		request.setAttribute("totalShippingCost", Item.getTotalShippingCost());

		RequestDispatcher rd = request.getRequestDispatcher("shipped.jsp");
		rd.forward(request, response);

	}

}
