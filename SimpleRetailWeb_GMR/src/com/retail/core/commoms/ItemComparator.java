package com.retail.core.commoms;


import java.util.Comparator;

public class ItemComparator implements Comparator<Item> {

	@Override
	public int compare(Item i1, Item i2) {

		if (i1.getUpc().compareTo(i2.getUpc()) > 0) {
			return 1;
		}
		else if (i1.getUpc().compareTo(i2.getUpc()) < 0) {
			return -1;
		}
		else
			return 0;
	}

}
