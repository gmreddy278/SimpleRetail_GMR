package com.retail.core.commoms;


import java.math.BigInteger;
import java.util.List;

enum ShipMethod{AIR, GROUND };

public class Item {

	private BigInteger upc;
	private String description;
	private double price;
	private double weight;
	private ShipMethod shipMethod;
	private double shippingCost;
	public static double totalShippingCost;
	
	
	
	//private ShippingMethod shippingMethod;
	
	public static double getTotalShippingCost() {
		return totalShippingCost;
	}
	
	public static void setTotalShippingCost(double totalShippingCost) {
		Item.totalShippingCost = totalShippingCost;
	}

	public BigInteger getUpc() {
		return upc;
	}
	public String getDescription() {
		return description;
	}
	public double getPrice() {
		return price;
	}
	public double getWeight() {
		return weight;
	}
	public ShipMethod getShipMethod() {
		return shipMethod;
	}
	public Item(BigInteger upc, String description, double price, double weight, ShipMethod ShipMethod) {
		super();
		this.upc = upc;
		this.description = description;
		this.price = price;
		this.weight = weight;
		this.shipMethod = ShipMethod;
		/*ShippingMethod shippingMethod;
		if (this.getShipMethod()==ShipMethod.AIR) {
			shippingMethod = new AirShippingMethod();
		} else {
			shippingMethod = new GroundShippingMethod();
		}
		
		this.shippingCost = this.getItemShippingCost(shippingMethod);*/
		if (this.getShipMethod()==ShipMethod.AIR) {
			this.shippingCost = this.getItemShippingCost(new AirShippingMethod());
		} else {
			this.shippingCost = this.getItemShippingCost(new GroundShippingMethod());
		}
		
		
	}
	
	@Override
	public String toString() {
		return "Item [upc=" + upc + ", description=" + description + ", price=" + price + ", weight=" + weight
				+ ", ShipMethod=" + shipMethod + "]" + ", shippingCost=" + shippingCost + "]" ;
	}
	
	
	public double shippingTarrif() {
		return this.getPrice()*3/100;
	}
	
	
	public double getItemShippingCost(ShippingMethod shippingMethod) {
		double d = shippingMethod.getItemShippingCost(this);
		return d;
		
	}
	public double getShippingCost() {
		return shippingCost;
	}
	


	
	public static double totalShippingCost(List<Item> items) {
		double totalShippingCost = 0;
		
		for(Item item: items) {
			totalShippingCost += item.shippingCost; 
		}
		return totalShippingCost;
	}
}
