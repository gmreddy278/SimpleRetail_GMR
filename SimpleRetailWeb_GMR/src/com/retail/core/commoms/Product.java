package com.retail.core.commoms;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Product {

	// METHOD THAT RETURN LIST OF AVAILABL ITEMS
	public List<Item> getProductsInfo() {

		List<Item> items = new ArrayList<>();

		items.add(new Item(new BigInteger("567321101987"), "CD � Pink Floyd, Dark Side Of The Moon", 19.99, 0.58, ShipMethod.AIR));
		items.add(new Item(new BigInteger("567321101986"), "CD � Beatles, Abbey Road", 17.99, 0.61, ShipMethod.GROUND));
		items.add(new Item(new BigInteger("567321101985"), "CD � Queen, A Night at the Opera", 20.49, 0.55,ShipMethod.AIR));
		items.add(new Item(new BigInteger("567321101984"), "CD � Michael Jackson, Thriller", 23.88, 0.50,ShipMethod.GROUND));
		items.add(new Item(new BigInteger("467321101899"), "iPhone - Waterproof Case", 9.75, 0.73, ShipMethod.AIR));
		items.add(new Item(new BigInteger("477321101878"), "iPhone -  Headphones", 17.25, 3.21, ShipMethod.GROUND));

		items.sort(new ItemComparator());
		
		return items;
	}
}
