package com.retail.core.commoms;


public class AirShippingMethod implements ShippingMethod{

	@Override
	public double getItemShippingCost(Item item) {
		char s = item.getUpc().toString().charAt(10);
		double d = Math.round(item.getWeight() * Character.getNumericValue(s) *100.0)/100.0;
		return d;
	}

	@Override
	public double getItemShippingCostWithTariff(Item item) {
		return this.getItemShippingCost(item)+((item.getPrice()*3)/100.0);
		
	}

	

	
	

}
