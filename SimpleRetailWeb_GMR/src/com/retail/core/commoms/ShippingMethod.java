package com.retail.core.commoms;


public interface ShippingMethod {
	
	public double getItemShippingCost(Item item);
	
	public double getItemShippingCostWithTariff(Item item);
}
